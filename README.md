![](https://assets.emo.gg/logo/emoteville-gold3-small.png)
<br>
[Website](https://go.emo.gg/) - [Discord](https://go.emo.gg/discord) - [Issue Tracker](https://go.emo.gg/issue-tracker)

<br>
EmoteVilleTBAG is a spigot plugin that allows the player to crouch on crops to grow them and not trample crops using energy, this is upgradable by creating different groups in the config.

The original documentation for this project can be found [HERE](https://www.evernote.com/shard/s627/sh/a09befec-a593-42a0-a9c1-0f892da49979/0ca9e9e7260b1bc08e95e117f57f95e6).

It is possible to interact with other plugins using the commands and placeholders as an input/output system, this is how most of our plugins work.


**_Commands_**

| Command | Description |
| ------ | ------ |
| /tbag  | Enable/Disable your Fisher status. |
| /tbag (player) | Force a player to enable/disable their status. |

**_Permissions_**

| Command | Description |
| ------ | ------ |
| emoteville.tbag.(group) | Applies selected group from from config, if you enable/disable the feature then it re-checks the player group. |

**_Placeholders_**

| Placeholder | Description |
| ------ | ------ |
| %tbag_status% | Returns ON/OFF depending on if you have it toggled.|
| %tbag_energy% | Returns players current energy. |
| %tbag_energy_max% | Returns max amount of energy for player. |
| %tbag_regen% | Returns regen percent out of 100. |
| %tbag_regenbar_100% | Return 100 ":" grey which change to green to highlight your prevent progess to full regen. (https://i.imgur.com/UqT2wmC.png). |
| %tbag_regenbar_50% | Return 50 ":" grey which change to green to highlight your prevent progess to full regen.(https://i.imgur.com/UqT2wmC.png). |
| %tbag_regenbar_25% | Return 25 ":" grey which change to green to highlight your prevent progess to full regen.(https://i.imgur.com/UqT2wmC.png). |


