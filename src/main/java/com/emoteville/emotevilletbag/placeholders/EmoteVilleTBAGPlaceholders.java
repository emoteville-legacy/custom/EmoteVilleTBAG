package com.emoteville.emotevilletbag.placeholders;

import com.emoteville.emotevilletbag.EmoteVilleTBAG;
import com.emoteville.emotevilletbag.utils.TBAGPlayer;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class EmoteVilleTBAGPlaceholders extends PlaceholderExpansion {

    private EmoteVilleTBAG plugin;

    public EmoteVilleTBAGPlaceholders(EmoteVilleTBAG plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean persist() {
        return true;
    }


    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return plugin.getDescription().getAuthors().toString();
    }

    @Override
    public String getIdentifier(){
        return "tbag";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier){

        if (player == null){
            return "";
        }

        TBAGPlayer tbagPlayer = plugin.stateManager.get(player.getUniqueId());
        if (tbagPlayer == null)
            return null;
        int maxEnergy = plugin.getConfig().getInt("groups." + tbagPlayer.getGroupName() + ".energy");

        // Add %tbag_status% placeholder
        if (identifier.equals("status"))
            return tbagPlayer.isActive() ? "ON" : "OFF";

        // Add %tbag_group% placeholder
        if (identifier.equals("group"))
            return tbagPlayer.getGroupName();

        // Add %tbag_radius% placeholder
        if (identifier.equals("radius"))
            return Integer.toString(tbagPlayer.getBlockRadius());

        // Add %tbag_energy% placeholder
        if (identifier.equals("energy"))
            return Integer.toString(tbagPlayer.getEnergy());

        // Add %tbag_energy_max% placeholder
        if (identifier.equals("energy_max"))
            return Integer.toString(plugin.getConfig().getInt("groups." + tbagPlayer.getGroupName() + ".energy"));

        // Add %tbag_regen% placeholder
        if (identifier.equals("regen"))
            return Double.toString(tbagPlayer.getRegenPercentage(maxEnergy, 100));

        // Add %tbag_regenbar_100% placeholder
        if (identifier.equals("regenbar_100")) {
            StringBuilder percentageBar = new StringBuilder();
            double regenBarPercentage = tbagPlayer.getRegenPercentage(maxEnergy, 100);
            for (int i = 0; i < 100; i++) {
                if (i < regenBarPercentage)
                    percentageBar.append(ChatColor.GREEN + "|");
                else
                    percentageBar.append(ChatColor.GRAY + "|");
            }
            return percentageBar.toString();
        }

        // Add %tbag_regenbar_50% placeholder
        if (identifier.equals("regenbar_50")) {
            StringBuilder percentageBar = new StringBuilder();
            double regenBarPercentage = tbagPlayer.getRegenPercentage(maxEnergy, 50);

            for (int i = 0; i < 50; i++) {
                if (i < regenBarPercentage)
                    percentageBar.append(ChatColor.GREEN + "|");
                else
                    percentageBar.append(ChatColor.GRAY + "|");
            }
            return percentageBar.toString();
        }

        // Add %tbag_regenbar_25% placeholder
        if (identifier.equals("regenbar_25")) {
            StringBuilder percentageBar = new StringBuilder();
            double regenBarPercentage = tbagPlayer.getRegenPercentage(maxEnergy, 25);

            for (int i = 0; i < 25; i++) {
                if (i < regenBarPercentage)
                    percentageBar.append(ChatColor.GREEN + "|");
                else
                    percentageBar.append(ChatColor.GRAY + "|");
            }
            return percentageBar.toString();
        }

        // Return null as the default case if placeholder does not exist
        return null;
    }
}
