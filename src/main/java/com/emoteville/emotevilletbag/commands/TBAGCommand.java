package com.emoteville.emotevilletbag.commands;

import com.emoteville.emotevilletbag.EmoteVilleTBAG;
import com.emoteville.emotevilletbag.utils.TBAGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class TBAGCommand implements CommandExecutor {

    private EmoteVilleTBAG plugin;

    public TBAGCommand(EmoteVilleTBAG plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (command.getName().equalsIgnoreCase("tbag")) {

            // Toggle the player's TBAG state
            if (args.length == 0) {

                // Check that the player has permission to use command
                if (!sender.hasPermission("emoteville.tbag")) {
                    sender.sendMessage(plugin.color(
                        plugin.getConfig().getString("messages.noPermission")
                        .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                    return false;
                }

                // Check that the sender is a player
                if (!(sender instanceof Player)) {
                    sender.sendMessage(plugin.color(
                        plugin.getConfig().getString("messages.playerOnlyCommand")
                        .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                    return false;
                }


                // Check if player already exists in the state manager.
                // If the player does, toggle their active status.
                // If the player does not, add them to the state manager with a status of ON.
                Player player = (Player) sender;
                UUID uuid = player.getUniqueId();
                if (plugin.stateManager.containsKey(uuid)) {

                    // Retrieved the cached player data in the state manager.
                    TBAGPlayer cached = plugin.stateManager.get(uuid);
                    TBAGPlayer updatedTBAGPlayer = cached;

                    // Update the player data if their permission group name has changed.
                    String currentGroupName = plugin.determineGroupNameViaPermission(player);
                    if (!updatedTBAGPlayer.getGroupName().equals(currentGroupName)) {

                        // Pull group information from config.yml
                        int blockRadius = plugin.getConfig().getInt("groups." + currentGroupName + ".radius");
                        int energy = plugin.getConfig().getInt("groups." + currentGroupName + ".energy");
                        int cost = plugin.getConfig().getInt("groups." + currentGroupName + ".cost");
                        int regen = plugin.getConfig().getInt("groups." + currentGroupName + ".regen");
                        String particle = plugin.getConfig().getString("groups." + currentGroupName + ".particle.type");

                        // Update the TBAGPlayer class with new group information
                        updatedTBAGPlayer.setGroupName(currentGroupName);
                        updatedTBAGPlayer.setBlockRadius(blockRadius);
                        updatedTBAGPlayer.setEnergy(energy);
                        updatedTBAGPlayer.setCost(cost);
                        updatedTBAGPlayer.setRegen(regen);
                        updatedTBAGPlayer.setParticle(particle);
                    }

                    // Toggle the player's current status
                    updatedTBAGPlayer.setActive(!updatedTBAGPlayer.isActive());

                    // Save the updated TBAGPlayer class to the state manager
                    plugin.stateManager.put(uuid, updatedTBAGPlayer);
                    player.sendMessage(plugin.color(
                        updatedTBAGPlayer.isActive()
                        ?   plugin.getConfig().getString("messages.statusON")
                            .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                        :   plugin.getConfig().getString("messages.statusOFF")
                            .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                } else {
                    plugin.addPlayerToStateManager(player, true);
                    player.sendMessage(plugin.color(
                        plugin.getConfig().getString("messages.statusON")
                        .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                }

                return true;
            }

            // Toggle the target player's TBAG state
            if (args.length == 1) {

                // Check that the player has permission to use command
                if (!sender.hasPermission("emoteville.tbag.other")) {
                    sender.sendMessage(plugin.color(
                        plugin.getConfig().getString("messages.noPermission")
                        .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                    return false;
                }

                // Check that the target player exists and is online.
                Player targetPlayer = Bukkit.getPlayer(args[0]);
                if (targetPlayer == null) {
                    sender.sendMessage(plugin.color(
                        plugin.getConfig().getString("messages.playerDoesNotExist")
                        .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                    return false;
                }

                // Check if player already exists in the state manager.
                // If the player does, toggle their active status.
                // If the player does not, add them to the state manager with a status of ON.
                if (plugin.stateManager.containsKey(targetPlayer.getUniqueId())) {

                    // Retrieved the cached player data in the state manager.
                    TBAGPlayer cached = plugin.stateManager.get(targetPlayer.getUniqueId());
                    TBAGPlayer updatedTBAGPlayer = cached;

                    // Update the player data if their permission group name has changed.
                    String currentGroupName = plugin.determineGroupNameViaPermission(targetPlayer);
                    if (!updatedTBAGPlayer.getGroupName().equals(currentGroupName)) {

                        // Pull group information from config.yml
                        int blockRadius = plugin.getConfig().getInt("groups." + currentGroupName + ".radius");
                        int energy = plugin.getConfig().getInt("groups." + currentGroupName + ".energy");
                        int cost = plugin.getConfig().getInt("groups." + currentGroupName + ".cost");
                        int regen = plugin.getConfig().getInt("groups." + currentGroupName + ".regen");
                        String particle = plugin.getConfig().getString("groups." + currentGroupName + ".particle.type");

                        // Update the TBAGPlayer class with new group information
                        updatedTBAGPlayer.setGroupName(currentGroupName);
                        updatedTBAGPlayer.setBlockRadius(blockRadius);
                        updatedTBAGPlayer.setEnergy(energy);
                        updatedTBAGPlayer.setCost(cost);
                        updatedTBAGPlayer.setRegen(regen);
                        updatedTBAGPlayer.setParticle(particle);
                    }

                    // Toggle the player's current status
                    updatedTBAGPlayer.setActive(!updatedTBAGPlayer.isActive());

                    // Save the updated TBAGPlayer class to the state manager
                    plugin.stateManager.put(targetPlayer.getUniqueId(), updatedTBAGPlayer);
                    targetPlayer.sendMessage(plugin.color(
                        updatedTBAGPlayer.isActive()
                            ?   plugin.getConfig().getString("messages.statusON")
                            .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                            :   plugin.getConfig().getString("messages.statusOFF")
                            .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                } else {
                    plugin.addPlayerToStateManager(targetPlayer, true);
                    targetPlayer.sendMessage(plugin.color(
                        plugin.getConfig().getString("messages.statusON")
                        .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                    sender.sendMessage(plugin.color(
                        plugin.getConfig().getString("messages.tbagOtherSuccessful")
                        .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                    ));
                }
                return true;
            }
            return true;
        }
        return false;
    }
}
