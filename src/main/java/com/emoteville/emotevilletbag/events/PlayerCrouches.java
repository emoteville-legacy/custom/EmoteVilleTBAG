package com.emoteville.emotevilletbag.events;

import com.emoteville.emotevilletbag.EmoteVilleTBAG;
import com.emoteville.emotevilletbag.utils.TBAGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PlayerCrouches implements Listener {

    private HashMap<UUID, Integer> regenManager = new HashMap<>();
    private EmoteVilleTBAG plugin;

    public PlayerCrouches(EmoteVilleTBAG plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerCrouch(PlayerToggleSneakEvent e) {
        Player player = e.getPlayer();
        Location location = e.getPlayer().getLocation();
        Location cropLocation = new Location(location.getWorld(), location.getX(), location.getY() + 1, location.getZ());

        if (e.isSneaking() && !e.isCancelled() && plugin.stateManager.containsKey(player.getUniqueId())) {
            TBAGPlayer tbagPlayer = plugin.stateManager.get(player.getUniqueId());
            List<Block> nearbyBlocks = getNearbyBlocks(cropLocation, tbagPlayer.getBlockRadius());
            int maxEnergy = plugin.getConfig().getInt("groups." + tbagPlayer.getGroupName() + ".energy");

            // If their TBAG status is not active, do nothing.
            if (!tbagPlayer.isActive())
                return;

            // If player does not have enough energy, notify them.
            if (tbagPlayer.getEnergy() < tbagPlayer.getCost()) {
                player.sendMessage(plugin.color(
                    plugin.getConfig().getString("messages.notEnoughEnergy")
                    .replace("%prefix%", plugin.getConfig().getString("messages.prefix"))
                ));
                return;
            }

            // Retrieve the list of blocks that are ageable.
            List<Block> ageableBlocks = new ArrayList<>();
            for (Block block : nearbyBlocks) {
                if (block != null && block.getBlockData() instanceof Ageable)
                    ageableBlocks.add(block);
            }

            // If the list is empty, do nothing.
            if (ageableBlocks.size() < 1)
                return;

            // Apply the growing effect on blocks that are not at their maximum age.
            // Spawn a particle effect on each block if the growing effect is applied.
            boolean isGrowingEffectApplied = false;
            for (Block block : ageableBlocks) {
                Ageable age = (Ageable) block.getBlockData();

                // Account for maximum block age.
                if (!(age.getAge() + 1 > age.getMaximumAge())) {
                    age.setAge(age.getAge() + 1);
                    block.setBlockData(age);
                    isGrowingEffectApplied = true;
                } else {
                    continue;
                }

                // Retrieve the particle effect configurations.
                String particleType = tbagPlayer.getParticle();
                int size = plugin.getConfig().getInt("groups." + tbagPlayer.getGroupName() + ".particle.dustOptions.size");
                int red = plugin.getConfig().getInt("groups." + tbagPlayer.getGroupName() + ".particle.dustOptions.color.red");
                int blue = plugin.getConfig().getInt("groups." + tbagPlayer.getGroupName() + ".particle.dustOptions.color.blue");
                int green = plugin.getConfig().getInt("groups." + tbagPlayer.getGroupName() + ".particle.dustOptions.color.green");

                // Fire the particle effect on all blocks affected.
                // If the particle effect is REDSTONE, it requires additional configuration.
                if (particleType.equals("REDSTONE")) {
                    Color color = Color.fromRGB(red, blue, green);
                    Particle.DustOptions dustOptions = new Particle.DustOptions(color, size);
                    player.getWorld().spawnParticle(Particle.REDSTONE, block.getLocation(), 0, 0, 0 ,0 ,0, dustOptions);
                } else {
                    player.getWorld().spawnParticle(Particle.valueOf(particleType), block.getLocation(), 0, 0, 0 ,0 ,0);
                }
            }

            // If the growing effect was applied, we need to remove the cost from their energy
            // and start the regeneration task.
            if (isGrowingEffectApplied) {

                // Subtract the cost from the player's energy.
                tbagPlayer.setEnergy(tbagPlayer.getEnergy() - tbagPlayer.getCost());

                // Create timer to refresh the player's energy after X seconds.
                // The rate of recovery is defined by regen/energy.
                if (!regenManager.containsKey(player.getUniqueId())) {
                    int taskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, () -> {

                        // Check if energy is at max. If so, end this task.
                        if (tbagPlayer.getEnergy() == maxEnergy) {
                            Bukkit.getScheduler().cancelTask(regenManager.get(player.getUniqueId()));
                            regenManager.remove(player.getUniqueId());
                            return;
                        }

                        // Restore player energy.
                        tbagPlayer.setEnergy(tbagPlayer.getEnergy() + 1);

                    }, 0, (tbagPlayer.getRegen() / maxEnergy) * 20L);

                    // Store UUID to regenManager to prevent multiple delayed tasks for one player.
                    regenManager.put(player.getUniqueId(), taskId);
                }

            }
        }
    }

    // Retrieve all x by x blocks around the block the player is standing on
    public List<Block> getNearbyBlocks(Location location, int radius) {
        List<Block> blocks = new ArrayList<>();
        for(int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
                for(int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, location.getBlockY(), z));
                }
        }
        return blocks;
    }
}
