package com.emoteville.emotevilletbag.events;

import com.emoteville.emotevilletbag.EmoteVilleTBAG;
import com.emoteville.emotevilletbag.utils.TBAGPlayer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerJumpsOnCrops implements Listener {

    private EmoteVilleTBAG plugin;

    public PlayerJumpsOnCrops(EmoteVilleTBAG plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJump(PlayerInteractEvent e) {
        Player player = e.getPlayer();

        // Prevent the player from jump breaking crops if their status is ON.
        if (plugin.stateManager.containsKey(player.getUniqueId())) {
            TBAGPlayer tbagPlayer = plugin.stateManager.get(player.getUniqueId());
            if (tbagPlayer.isActive()) {
                if (e.getAction() == Action.PHYSICAL && e.getClickedBlock().getType() == Material.FARMLAND)
                    e.setCancelled(true);
            }
        }
    }
}
