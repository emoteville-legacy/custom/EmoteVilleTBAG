package com.emoteville.emotevilletbag.events;

import com.emoteville.emotevilletbag.EmoteVilleTBAG;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoins implements Listener {

    private EmoteVilleTBAG plugin;

    public PlayerJoins(EmoteVilleTBAG plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        // Check if the player already exists in the state manager. If so, do nothing.
        if (plugin.stateManager.containsKey(player.getUniqueId()))
            return;

        // By default, place all players when they join into the state manager with status OFF.
        plugin.addPlayerToStateManager(player, false);
    }
}
