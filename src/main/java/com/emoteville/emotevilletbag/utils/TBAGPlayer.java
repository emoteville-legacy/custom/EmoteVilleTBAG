package com.emoteville.emotevilletbag.utils;

public class TBAGPlayer {

    private String groupName;
    private int blockRadius;
    private int energy;
    private int cost;
    private int regen;
    private String particle;
    private boolean isActive;

    public TBAGPlayer(String groupName, int blockRadius, int energy, int cost, int regen, String particle, boolean isActive) {
        this.groupName = groupName;
        this.blockRadius = blockRadius;
        this.energy = energy;
        this.cost = cost;
        this.regen = regen;
        this.particle = particle;
        this.isActive = isActive;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getBlockRadius() {
        return blockRadius;
    }

    public void setBlockRadius(int blockRadius) {
        this.blockRadius = blockRadius;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getRegen() {
        return regen;
    }

    public void setRegen(int regen) {
        this.regen = regen;
    }

    public String getParticle() {
        return particle;
    }

    public void setParticle(String particle) {
        this.particle = particle;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "TBAGPlayer{" +
                "groupName='" + groupName + '\'' +
                ", blockRadius=" + blockRadius +
                ", energy=" + energy +
                ", cost=" + cost +
                ", regen=" + regen +
                ", particle='" + particle + '\'' +
                ", isActive='" + isActive + '\'' +
                '}';
    }

    public Double getRegenPercentage(int maxEnergy, int proportion) {
        return (energy / (double) maxEnergy) * proportion;
    }
}
