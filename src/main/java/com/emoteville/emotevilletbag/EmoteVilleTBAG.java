package com.emoteville.emotevilletbag;

import com.emoteville.emotevilletbag.commands.TBAGCommand;
import com.emoteville.emotevilletbag.events.*;
import com.emoteville.emotevilletbag.placeholders.EmoteVilleTBAGPlaceholders;
import com.emoteville.emotevilletbag.utils.TBAGPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

public final class EmoteVilleTBAG extends JavaPlugin {

    public HashMap<UUID, TBAGPlayer> stateManager = new HashMap<>();
    HashMap<String, Integer> groupPriority = new HashMap<>();

    @Override
    public void onEnable() {
        // Save the default configuration.
        saveDefaultConfig();

        // Establish the priority order of configuration group names.
        int priority = 1;
        for (String section : getConfig().getConfigurationSection("groups").getKeys(false)) {
            groupPriority.put(section, priority);
            priority++;
        }

        // Register all the plugin commands.
        getCommand("tbag").setExecutor(new TBAGCommand(this));

        // Register all the plugin events.
        Bukkit.getPluginManager().registerEvents(new PlayerJoins(this), this);
        Bukkit.getPluginManager().registerEvents(new PlayerCrouches(this), this);
        Bukkit.getPluginManager().registerEvents(new PlayerJumpsOnCrops(this), this);

        // Register the EmoteVilleTBAG placeholders if the Placeholder API plugin is present
        if (this.getServer().getPluginManager().isPluginEnabled("PlaceholderAPI"))
            new EmoteVilleTBAGPlaceholders(this).register();
    }

    @Override
    public void onDisable() {
        stateManager.clear();
    }

    public String color(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    public void addPlayerToStateManager(Player player, boolean isActive) {
        // Determine the player's group name by permission.
        // Retrieve that groups' configuration with this name.
        String groupName = determineGroupNameViaPermission(player);

        // Check that the group name from the player's permission is valid
        if (!getConfig().contains("groups." + groupName)) {
            player.sendMessage("Invalid group name permission. Toggle did not fire, please notify staff!");
            return;
        }

        // Pull group information from config.yml
        int blockRadius = getConfig().getInt("groups." + groupName + ".radius");
        int energy = getConfig().getInt("groups." + groupName + ".energy");
        int cost = getConfig().getInt("groups." + groupName + ".cost");
        int regen = getConfig().getInt("groups." + groupName + ".regen");
        String particle = getConfig().getString("groups." + groupName + ".particle.type");
        TBAGPlayer tbagPlayer = new TBAGPlayer(groupName, blockRadius, energy, cost, regen, particle, isActive);
        stateManager.put(player.getUniqueId(), tbagPlayer);
    }

    public String determineGroupNameViaPermission(Player player) {
        String groupName = "";

        // Loop over the player's permissions. Parse the group name from existing permission
        int currentPermissionPriority = 0;
        Set<PermissionAttachmentInfo> effectPerms = player.getEffectivePermissions();
        for (PermissionAttachmentInfo perm : effectPerms) {
            if (perm.getPermission().contains("emoteville.tbag.")) {
                if (groupPriority.get(perm.getPermission().split("emoteville.tbag.")[1].toLowerCase()) > currentPermissionPriority) {
                    groupName = perm.getPermission().split("emoteville.tbag.")[1].toLowerCase();
                    currentPermissionPriority = groupPriority.get(groupName);
                }
            }
        }
        return groupName;
    }

}
